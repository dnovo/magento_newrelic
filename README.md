# README #

### New Relic Reporting ###
The Official Magento New Relic Reporting extension allows you to easily integrate New Relic APM and New Relic Insights
with Magento, giving you real-time visibility into business and performance metrics for data-driven decision making.
The extension works with both Magento Enterprise Edition and Magento Community Edition.


### Description ###
To be able to use this module in projects we created a modman based version of it.

* Source: https://www.magentocommerce.com/magento-connect/extension-29.html
* Version 1.1.0


### Updates ###
Version 1.1.1
* MEX-5: Notice: Array to string conversion fix


### Contact ###
* Maintainer: dNovo ICT
* contact info@dnovo.nl
* web: www.dNovo.nl